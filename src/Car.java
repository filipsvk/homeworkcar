import javax.annotation.processing.SupportedSourceVersion;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class Car {
    public static void main(String[] args) {
        int Lamborghini = 0;
        int SixBySix = 0;
        int Morgan = 0;
        int Kia = 0;
        Scanner sc = new Scanner(System.in);
        HashMap<String, String> Cylinders = new HashMap<String, String>();
        Cylinders.put("Lamborghini", "12");
        Cylinders.put("SixBySix", "8");
        Cylinders.put("Morgan", "2");
        Cylinders.put("Kia", "6");

        HashMap<String, String> Doors = new HashMap<String, String>();
        Doors.put("Lamborghini", "2");
        Doors.put("SixBySix", "4");
        Doors.put("Morgan", "0");
        Doors.put("Kia", "4");

        HashMap<String, String> Wheels = new HashMap<String, String>();
        Wheels.put("Lamborghini", "4");
        Wheels.put("SixBySix", "6");
        Wheels.put("Morgan", "3");
        Wheels.put("Kia", "4");

        HashMap<String, String> Seats = new HashMap<String, String>();
        Seats.put("Lamborghini", "2");
        Seats.put("SixBySix", "5");
        Seats.put("Morgan", "2");
        Seats.put("Kia", "4");

        HashMap<String, String> Purpose = new HashMap<String, String>();
        Purpose.put("Lamborghini", "Speed");
        Purpose.put("SixBySix", "ShowingOff");
        Purpose.put("Morgan", "Fun");
        Purpose.put("Kia", "Practicality");

        Iterator<String> itr1 = Cylinders.values().iterator();
        System.out.println("How many cylinders u want? Choose out of: ");
        while (itr1.hasNext()) {
            System.out.print(itr1.next() + " ");
        }
        String s1 = sc.nextLine();
        if (s1.equals(Cylinders.get("Lamborghini"))) {
            Lamborghini++;
        } else if (s1.equals(Cylinders.get("SixBySix"))) {
            SixBySix++;
        } else if (s1.equals(Cylinders.get("Morgan"))) {
            Morgan++;
        } else if (s1.equals(Cylinders.get("Kia"))) {
            Kia++;
        } else {
            System.out.println("U didnt choose out of range and ruined the whole build... start again");
            System.exit(-1);
        }
        Iterator<String> itr2 = Doors.values().iterator();
        System.out.println("How many doors u want? Choose out of: ");
        while (itr2.hasNext()) {
            System.out.print(itr2.next() + " ");
        }
        String s2 = sc.nextLine();
        if (s2.equals(Doors.get("Lamborghini"))) {
            Lamborghini++;
        } else if (s2.equals(Doors.get("SixBySix"))) {
            SixBySix++;
            Kia++;
        } else if (s2.equals(Doors.get("Morgan"))) {
            Morgan++;
        } else {
            System.out.println("U didnt choose out of range and ruined the whole build... start again");
            System.exit(-1);
        }
        Iterator<String> itr3 = Wheels.values().iterator();
        System.out.println("How many wheels u want? Choose out of: ");
        while (itr3.hasNext()) {
            System.out.print(itr3.next() + " ");
        }
        String s3 = sc.nextLine();
        if (s3.equals(Wheels.get("Lamborghini"))) {
            Lamborghini++;
            Kia++;
        } else if (s3.equals(Wheels.get("SixBySix"))) {
            SixBySix++;
        } else if (s3.equals(Wheels.get("Morgan"))) {
            Morgan++;
        } else {
            System.out.println("U didnt choose out of range and ruined the whole build... start again");
            System.exit(-1);
        }
        Iterator<String> itr4 = Seats.values().iterator();
        System.out.println("How many Seats u want? Choose out of: ");
        while (itr4.hasNext()) {
            System.out.print(itr4.next() + " ");
        }
        String s4 = sc.nextLine();
        if (s4.equals(Seats.get("Lamborghini"))) {
            Lamborghini++;
            Morgan++;
        } else if (s4.equals(Seats.get("SixBySix"))) {
            SixBySix++;
        } else if (s4.equals(Seats.get("Kia"))) {
            Kia++;
        } else {
            System.out.println("U didnt choose out of range and ruined the whole build... start again");
            System.exit(-1);
        }
        Iterator<String> itr6 = Purpose.values().iterator();
        System.out.println("What purpose should the car have? Choose out of: ");
        while (itr6.hasNext()) {
            System.out.print(itr6.next() + " ");
        }
        String s6 = sc.nextLine();
        if (s6.equals(Purpose.get("Lamborghini"))) {
            Lamborghini++;
        } else if (s6.equals(Purpose.get("SixBySix"))) {
            SixBySix++;
        } else if (s6.equals(Purpose.get("Morgan"))) {
            Morgan++;
        } else if (s6.equals(Purpose.get("Kia"))) {
            Kia++;
        } else {
            System.out.println("U didnt answered with available options and ruined the whole build almost at the end... start again");
            System.exit(-1);
        }
        System.out.println("Can u spend more than 60k? (Yes/No question)");
        String s5 = sc.nextLine();
        if (s5.equals("Yes")) {
                Lamborghini++;
                SixBySix++;
                Morgan = 0;
                Kia = 0;
                if (Lamborghini > SixBySix) {
                    System.out.println("Get a Lamborghini Aventador. Configuration: ");
                    System.out.println("Cylinders: " + Cylinders.get("Lamborghini"));
                    System.out.println("Doors: " + Doors.get("Lamborghini"));
                    System.out.println("Seats: " + Seats.get("Lamborghini"));
                    System.out.println("Wheels: " + Wheels.get("Lamborghini"));
                    System.out.println("Purpose: " + Purpose.get("Lamborghini"));
                } else if (Lamborghini < SixBySix) {
                    System.out.println("Get a Mercedes G wagon 6x6. Configuration:");
                    System.out.println("Cylinders: " + Cylinders.get("SixBySix"));
                    System.out.println("Doors: " + Doors.get("SixBySix"));
                    System.out.println("Seats: " + Seats.get("SixBySix"));
                    System.out.println("Wheels: " + Wheels.get("SixBySix"));
                    System.out.println("Purpose: " + Purpose.get("SixBySix"));
                } else {
                    System.out.println("Get a Lamborghini Aventador or a Mercedes G wagon 6x6. For one car answer run the test again and change one choice.");
                }
            } else if (s5.equals("No")) {
                Morgan++;
                Kia++;
                Lamborghini = 0;
                SixBySix = 0;
                if (Morgan > Kia) {
                    System.out.println("Get a Morgan 3-wheeler. Configuration: ");
                    System.out.println("Cylinders: " + Cylinders.get("Morgan"));
                    System.out.println("Doors: " + Doors.get("Morgan"));
                    System.out.println("Seats: " + Seats.get("Morgan"));
                    System.out.println("Wheels: " + Wheels.get("Morgan"));
                    System.out.println("Purpose: " + Purpose.get("Morgan"));

                } else if (Morgan < Kia) {
                    System.out.println("Get a Kia Stinger GT. Configuration:");
                    System.out.println("Cylinders: " + Cylinders.get("Kia"));
                    System.out.println("Doors: " + Doors.get("Kia"));
                    System.out.println("Seats: " + Seats.get("Kia"));
                    System.out.println("Wheels: " + Wheels.get("Kia"));
                    System.out.println("Purpose: " + Purpose.get("Kia"));
                } else {
                    System.out.println("Get a Morgan 3-wheeler or a Kia Stinger GT. For one car answer run the test again and change one choice.");
                }
            } else {
                System.out.println("U didnt answered with Yes/No and ruined the whole build at the end... start again");
                System.exit(-1);
            }
        }
    }


